/*
 * sqlite3_db.h
 *
 *  Created on: May 2, 2016
 *      Author: sherpa
 */
#ifndef SQLITE3_DB_H_
#define SQLITE3_DB_H_



#include <boost/log/common.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/sources/logger.hpp>

#include <string>

#include <sqlite3.h>



using namespace boost::log ;
using namespace std ;


class SQLite3_DB
{
	private:
		sqlite3 *db;
		char *zErrMsg;
		char **result;
		int rc;
		int nrow,ncol;
		int db_open;

	public:

		std::vector<std::string> vhead;
		std::vector<std::vector<std::string>> vdata;

		SQLite3_DB (std::string databaseName ) ;
		~SQLite3_DB() ;

		int exe( std::string s_exe ) ;
};



#endif /* SQLITE3_API_DB_H_ */
