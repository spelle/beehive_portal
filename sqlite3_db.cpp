/*
 * sqlite3_db.cpp
 *
 *  Created on: May 2, 2016
 *      Author: sherpa
 */



#include "sqlite3_db.h"



SQLite3_DB::SQLite3_DB( std::string databaseName )
		: zErrMsg(0), result(NULL), rc(0), nrow(0), ncol(0), db_open(0)
{
	rc = sqlite3_open( databaseName.c_str(), &db ) ;

	if( rc ){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		sqlite3_close(db);
	}

	db_open = 1 ;
}



SQLite3_DB::~SQLite3_DB() {
	sqlite3_close( db ) ;
}



int SQLite3_DB::exe(std::string s_exe)
{
	rc = sqlite3_get_table
			(
					db,				/* An open database */
					s_exe.c_str(),	/* SQL to be executed */
					&result,		/* Result written to a char *[]  that this points to */
					&nrow,			/* Number of result rows written here */
					&ncol,			/* Number of result columns written here */
					&zErrMsg		/* Error msg written here */
			);

	vhead.clear();

	vdata.clear();

	if( rc == SQLITE_OK ) {

		int i = 0 ;
		while( i < ncol ) {					//
			vhead.push_back( result[i] ) ;	// First row heading
			i ++ ;							//
		}

		while( i < ncol*nrow ) {

			static std::vector<std::string> vrow ;

			vrow.push_back( result[i] ) ;

			if( (ncol - 1) == (i % ncol) ) {
				vdata.push_back( vrow ) ;
				vrow.clear() ;
			}

			i++ ;
		}
	}

	sqlite3_free_table( result ) ;

	return rc ;
}



