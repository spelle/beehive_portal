


#include <signal.h>
#include <string.h>

#include <iostream>
#include <string>

#include <boost/log/common.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/core/null_deleter.hpp>
#include <boost/shared_ptr.hpp>

#include "libnavajo/libnavajo.hh"
#include "libnavajo/LogStdOutput.hh"
//#include "libnavajo/LogFile.hh"

#include "sqlite3_db.h"



using namespace boost::log ;



WebServer *webServer = NULL ;



void exitFunction( int dummy )
{
	if ( webServer != NULL )
		webServer->stopService() ;
}



class beehiveData_labels: public DynamicPage
{
	private :
		SQLite3_DB & theDb ;

	public:
		beehiveData_labels( SQLite3_DB & db ) :
			theDb( db )
		{

		}
		virtual ~beehiveData_labels()
		{

		}

	bool getPage( HttpRequest* request, HttpResponse *response )
	{
		std::string sql ;

		sql = "SELECT " ;
		sql += "\t\"TimeStamp\" " ;
		sql += "FROM" ;
		sql += "\tBeehivesData " ;
		sql += "\tORDER BY " ;
		sql += "\t\"TimeStamp\" " ;
		sql += "\tDESC LIMIT 50 ;" ;

		theDb.exe( sql ) ;

		std::string data ;

		data = "[" ;

		for ( auto row = std::begin(theDb.vdata); row != std::end(theDb.vdata); row++ )
		{
			data += "\""      ;
			data += (*row)[0] ;
			data += "\"" ;

			if( std::next(row) != std::end(theDb.vdata) )
				data += ", "    ;
		}

		data += "]" ;

		return fromString( data, response ) ;
	}
};



class beehiveData_seriess: public DynamicPage
{
	private :
		SQLite3_DB & theDb ;

	public:
		beehiveData_seriess( SQLite3_DB & db ) :
			theDb( db )
		{

		}
		virtual ~beehiveData_seriess()
		{

		}

	bool getPage( HttpRequest* request, HttpResponse *response )
	{
		std::string sql ;

		sql = "SELECT " ;
		sql += "\t\"TimeStamp\", " ;
		sql += "\t\"gaugeNo1Value\", " ;
		sql += "\t\"gaugeNo1RawValue\", " ;
		sql += "\t\"gaugeNo2Value\", " ;
		sql += "\t\"gaugeNo2RawValue\", " ;
		sql += "\t\"gaugeNo3Value\", " ;
		sql += "\t\"gaugeNo3RawValue\", " ;
		sql += "\t\"scaleValue\", " ;
		sql += "\t\"scaleRawValue\" " ;
		sql += "FROM" ;
		sql += "\tBeehivesData " ;
		sql += "\tORDER BY " ;
		sql += "\t\"TimeStamp\" " ;
		sql += "\tDESC LIMIT 1 ;" ;

		theDb.exe( sql ) ;

		std::string data ;

		data = "[" ;

		size_t i = 1 ;
		size_t ncol = theDb.vhead.size() ;
		while( i < ncol )
		{
			data += "\"" ;
			data += theDb.vhead[i] ;
			data += "\"" ;

			if( i < ncol - 1 )
				data += ", " ;

			i++ ;
		}

		data += "]" ;

		return fromString( data, response ) ;
	}
};



class beehiveData_data: public DynamicPage
{
	private :
		SQLite3_DB & theDb ;

	public:
		beehiveData_data( SQLite3_DB & db ) :
			theDb( db )
		{

		}
		virtual ~beehiveData_data()
		{

		}

	bool getPage( HttpRequest* request, HttpResponse *response )
	{
		std::string sql ;

		sql = "SELECT " ;
		sql += "\t\"TimeStamp\", " ;
		sql += "\t\"gaugeNo1Value\", " ;
		sql += "\t\"gaugeNo1RawValue\", " ;
		sql += "\t\"gaugeNo2Value\", " ;
		sql += "\t\"gaugeNo2RawValue\", " ;
		sql += "\t\"gaugeNo3Value\", " ;
		sql += "\t\"gaugeNo3RawValue\", " ;
		sql += "\t\"scaleValue\", " ;
		sql += "\t\"scaleRawValue\" " ;
		sql += "FROM" ;
		sql += "\tBeehivesData " ;
		sql += "\tORDER BY " ;
		sql += "\t\"TimeStamp\" " ;
		sql += "\tDESC LIMIT 50 ;" ;

		theDb.exe( sql ) ;

		std::string data ;

		data = "[\n" ;

		size_t i = 1 ;
		size_t ncol = theDb.vhead.size() ;
		while( i < ncol )
		{
			data += "\t[" ;

			for( auto row = std::begin(theDb.vdata); row != std::end(theDb.vdata); ++row )
			{
				data += (*row)[i] ;
				if( std::next(row) != std::end(theDb.vdata) )
					data += ", " ;
			}

			data += "]" ;

			if( i < ncol - 1 )
				data += ", " ;

			data += "\n" ;

			i++ ;
		}

		data += "]" ;

		// $scope.data = [
		//   [65, 59, 80, 81, 56, 55, 40],
		//   [28, 48, 40, 19, 86, 27, 90]
		// ];

//		for (auto row = std::begin(theDb.vdata); row != std::end(theDb.vdata); ++row )
//		{
//			for (auto col = std::begin(*row); col != std::end(*row); ++col )
//			{
//				data += *col ;
//				if( std::next(col) != std::end(*row) )
//					data += " | " ;
//			}
//			data += "\n" ;
//		}

		return fromString( data, response ) ;
	}
};



int main()
{
	// connect signals
	signal( SIGTERM, exitFunction );
	signal( SIGINT, exitFunction );

	NVJ_LOG->addLogOutput(new LogStdOutput);
	//NVJ_LOG->addLogOutput( new LogFile( "beehive_portal.log" ) ) ;

	webServer = new WebServer;

	typedef sinks::asynchronous_sink<sinks::text_ostream_backend> text_sink ;
	boost::shared_ptr<text_sink> sink = boost::make_shared<text_sink>() ;

	boost::shared_ptr<std::ostream> stream{&std::clog, boost::null_deleter{} } ;
	sink->locked_backend()->add_stream(stream) ;

	core::get()->add_sink(sink) ;

	sources::logger lg ;

	std::string db_file_name("../boost_asio_async_udp_beehive_server/beehives.db") ;

	SQLite3_DB theDb( db_file_name ) ;

	webServer->setServerPort(8443);
	//webServer->setThreadsPoolSize(1);

	//uncomment to switch to https
	//webServer->setUseSSL(true, "../crt.pem");

	//LocalRepository htmlLocalRepo("/html", "./html");
	//LocalRepository docsLocalRepo("/docs", "./docs");
	//LocalRepository xappLocalRepo("/xapp", "./xapp");
	LocalRepository chartLocalRepo("/", "./AngularChart");
	//webServer->addRepository(&htmlLocalRepo);
	//webServer->addRepository(&docsLocalRepo);
	//webServer->addRepository(&xappLocalRepo);
	webServer->addRepository(&chartLocalRepo);

	beehiveData_labels  myBeehiveData_labels ( theDb ) ;
	beehiveData_seriess myBeehiveData_series ( theDb ) ;
	beehiveData_data    mybeehiveData_data   ( theDb ) ;
	DynamicRepository dynRepo;
	dynRepo.add("/BeehivesData_labels.json",&myBeehiveData_labels); // unusual html extension for a dynamic page !
	dynRepo.add("/BeehivesData_series.json",&myBeehiveData_series);
	dynRepo.add("/BeehivesData_data.json",  &mybeehiveData_data);
	webServer->addRepository(&dynRepo);

	webServer->startService();

	// Your Processing here !
	//...
	webServer->wait();

	LogRecorder::freeInstance() ;

	return 0;
}
