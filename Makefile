

REPOSITORY				= html
OUTPUTFILENAME			= PrecompiledRepository.cc
NAVAJO_PRECOMPILER_EXEC	= navajoPrecompiler

UNAME := $(shell uname)
LBITS := $(shell getconf LONG_BIT)

LIB_DIR	= lib
CXX		= g++

LIBS		= -L../libnavajo/build/lib -L/home/sherpa/.opt/boost/stage/lib -lnavajo -lboost_log -lboost_log_setup -lboost_system -lboost_thread -lsqlite3 -lpthread -lz -lssl -lcrypto -ldl
DEFS		= -DLINUX -Wall -Wno-unused -fexceptions -fPIC -D_REENTRANT
CXXFLAGS	= -std=c++11 -O0 -g3  -Wdeprecated-declarations  -DBOOST_ALL_DYN_LINK 

CPPFLAGS	= \
	-I/home/sherpa/.opt/boost \
	-I../libnavajo/include

LD		=  g++

LDFLAGS	= -Wall -Wno-unused -O3 -Wl,-rpath ../libnavajo/build/lib -Wl,-rpath ~/.opt/boost/stage/lib

APP_NAME = beehive_portal

APP_OBJS = \
		sqlite3_db.o \
		beehive_portal.o
# PrecompiledRepository.o


#######################
# DEPENDENCE'S RULES  #
#######################

%.o: %.cc
	$(CXX) -c $< -o $@ $(CXXFLAGS) $(CPPFLAGS) $(DEFS)

all: $(APP_NAME)

debug: all
Release: all

PrecompiledRepository.o:
	@echo Generate Cpp files from HTML repository
	@rm -f $(OUTPUTFILENAME)
	@find . \( -name "*~" -o -name "*.old" -o -name "*.bak" \) -exec rm -f '{}' \;
	$(NAVAJO_PRECOMPILER_EXEC) $(REPOSITORY) >> $(OUTPUTFILENAME) ; cd ..
	$(CXX) -c PrecompiledRepository.cc -o $@ $(CXXFLAGS) $(CPPFLAGS) $(DEFS)

$(APP_NAME): $(APP_OBJS) $(LIB_STATIC_NAME)
	rm -f $@
	$(LD) $(LDFLAGS) -o $@ $(APP_OBJS) $(LIB_STATIC_NAME) $(LIBS)

run:
	./beehive_portal

clean:
	@rm -f $(OUTPUTFILENAME)
	@for i in $(APP_OBJS); do  rm -f $$i ; done
	@rm -f $(APP_NAME)
	@rm -f *.o
	@rm -f *.log
